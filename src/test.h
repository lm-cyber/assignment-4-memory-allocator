#ifndef TEST_H
#define TEST_H


#include <stdint.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

int all_test();

#endif

