#include "test.h"

void* heap;
struct block_header* block;

static int block_count(struct block_header* block) {
    int count = 0;
    struct block_header *iteration = block;
    while (iteration->next) {
        if (!iteration->is_free) count++;
        iteration = iteration->next;
    }
    return count;
}

static bool init_env() {
    heap = heap_init(REGION_MIN_SIZE);
    block = (struct block_header *) heap;
    if (!heap || !block)
        return false;
    return true;
}

static void test_1() {
    printf("Test 1\n");
    debug_heap(stdout, heap);
    void *test = _malloc(1024);
    debug_heap(stdout, heap);
    if (!test || block->capacity.bytes != 1024) {
        printf("Test 1 FAILED");
        _free(test);
    } else {
        _free(test);
        printf("Test 1 PASSED\n");
    }
}

static void test_2() {
    printf("Test 2");
    debug_heap(stdout, heap);
    void *test_1 = _malloc(1024);
    void *test_2 = _malloc(2048);
    debug_heap(stdout, heap);
    if (block_count(block) != 2) {
        printf("Test 2 FAILED");
        _free(test_1);
        _free(test_2);
    } else {
        _free(test_1);
        debug_heap(stdout, heap);
        if (block_count(block) == 1)
            printf("Test 2 FAILED");
        else
            printf("Test 2 PASSED");
        _free(test_2);
    }
}

static void test_3() {
    printf("Test 3");
    debug_heap(stdout, heap);
    void *test_1 = _malloc(1024);
    void *test_2 = _malloc(2048);
    void *test_3 = _malloc(4096);
    debug_heap(stdout, heap);
    if (block_count(block) != 3) {
        printf("Test 3 FAILED");
        _free(test_1);
        _free(test_2);
        _free(test_3);
    } else {
        _free(test_1);
        _free(test_2);
        debug_heap(stdout, heap);
        if (block_count(block) == 1)
            printf("Test 3 PASSED");
        else
            printf("Test 3 FAILED");
        _free(test_3);
    }
}

static void test_4() {
    printf("Test 4");
    debug_heap(stdout, heap);
    void *test_1 = _malloc(8192);
    void *test_2 = _malloc(4096);
    debug_heap(stdout, heap);
    if (block_count(block) != 2)
        printf("Test 4 FAILED");
    _free(test_1);
    _free(test_2);
    printf("Test 4 PASSED");
}

static void test_5() {
    printf("Test 5");
    void *mem1 = _malloc(2048);
    void *mem2 = _malloc(2048);
    debug_heap(stdout, heap);
    _free(mem1);
    _free(mem2);
    printf("Test 5 PASSED");
}



int all_test() {
    if (init_env()) {
        test_1();
        test_2();
        test_3();
        test_4();
        test_5();
        return 0;
    } else printf("Testing environment not initiated\n");
    return -1;
}
